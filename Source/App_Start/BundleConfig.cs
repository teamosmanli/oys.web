﻿using System.Web.Optimization;

namespace OYS.Web {
    public class BundleConfig {

        public static void RegisterBundles(BundleCollection bundles) {
           
        }

        internal static void RegisterDashboardBundles(BundleCollection bundles) {

            bundles.Add(
                new StyleBundle("~/Dashboard/Core/css").Include(
                    "~/Assets/css/site.css"
                )
            );
            bundles.Add(
                new StyleBundle("~/Dashboard/Vendors/css").Include(
                    "~/Assets/css/ui-lightness/jquery-ui-1.10.4.custom.min.css",
                    "~/Assets/css/bootstrap.min.css",
                    "~/Assets/vendors/animate.css/animate.css",
                    "~/Assets/vendors/jquery-pace/pace.css",
                    "~/Assets/vendors/iCheck/skins/all.css",
                    //"~/Assets/vendors/jquery-notific8/jquery.notific8.min.css",
                    "~/Assets/vendors/Assets/vendors/Assets/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css",
                    "~/Assets/vendors/Assets/vendors/Assets/vendors/calendar/zabuto_calendar.min.css"
                ).Include("~/Assets/css/font-awesome.min.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/bootstrap.css", new CssRewriteUrlTransform())
            );

            bundles.Add(
                new StyleBundle("~/Dashboard/Themes/css").Include(
                    "~/Assets/css/style-responsive.css"
                )
            );

            bundles.Add(
                new ScriptBundle("~/Dashboard/Global/scripts").Include(
                    "~/Assets/vendors/jquery-{version}.js",
                    "~/Assets/vendors/jquery-ui.js",
                    //"~/Assets/vendors/bootstrap.js",
                    "~/Assets/vendors/bootstrap/js/bootstrap.min.js",
                    "~/Assets/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js",
                    "~/Assets/vendors/metisMenu/jquery.metisMenu.js",
                    "~/Assets/vendors/slimScroll/jquery.slimscroll.js",
                    "~/Assets/vendors/jquery-cookie/jquery.cookie.js",
                    "~/Assets/vendors/iCheck/icheck.min.js",
                    "~/Assets/vendors/iCheck/custom.min.js",
                    //"~/Assets/vendors/jquery-notific8/jquery.notific8.min.js",
                    "~/Assets/vendors/holder/holder.js",
                    "~/Assets/vendors/responsive-tabs/responsive-tabs.js",
                    "~/Assets/vendors/jquery-news-ticker/jquery.newsTicker.min.js",
                    "~/Assets/vendors/moment/moment-with-locales.js",
                    "~/Assets/vendors/flot-chart/jquery.flot.js",
                    "~/Assets/vendors/DataTables/media/js/jquery.dataTables.js",
                    "~/Assets/vendors/extensions.js"
                )
            );
            bundles.Add(
                new ScriptBundle("~/Dashboard/App/scripts")
                .Include(
                    "~/Assets/vendors/angular/angular.min.js",
                    "~/Assets/vendors/angular/angular-route.min.js",
                    "~/Assets/vendors/angular/angular-ui-router.js",
                    "~/Assets/vendors/angular/ocLazyLoad.js",
                    "~/Assets/vendors/bootstrap/ui-bootstrap.js",
                    "~/Angular/App/app.js")
                .IncludeDirectory("~/Angular/App/directives", "*.js", true)
                .IncludeDirectory("~/Angular/App/services", "*.js", true)
                .IncludeDirectory("~/Angular/App/factories", "*.js", true)
                .IncludeDirectory("~/Angular/App/filters", "*.js", true)
                .IncludeDirectory("~/Angular/App/controllers", "*.js", true)
                .IncludeDirectory("~/Angular/App/generic-controllers", "*.js", true)
                .IncludeDirectory("~/Angular/App/components", "*.js", true)
            );
        }

    }
}

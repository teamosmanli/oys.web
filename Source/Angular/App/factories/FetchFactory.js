﻿App.factory('FetchFactory', function (MenuItemsFactory) {
    return {
        MenuItems: MenuItemsFactory
    };
});
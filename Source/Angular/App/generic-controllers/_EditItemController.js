﻿App.controller('EditItemController', ['$scope', '$state', '$stateParams', 'HttpService', 'BusService', 'QService', function ($scope, $state, $stateParams, hs, bus, qs) {

    $scope.domain = $stateParams.domain;
    $scope.state = $state.current;
    $scope.params = $stateParams;
    $scope.command = $stateParams.command;

    qs.setScope($scope);

    bus.onGetReceived($scope, $scope.domain + ':one', function (data) {
        $scope.model = data.SingleResponse;
        prepare();
    });

    bus.onGetReceived($scope, 'editframe:fields', function (data) {
        $scope.fieldmodel = data;
    });

    bus.onPutReceived($scope, $scope.domain + ':put', function (data) {
        qs.announceDone("Değiştirme Başarılı");
    }, function (data) {
        qs.announceFailed(data.Message);
    });

    function init() {
        $('#edit-container').loading(function (lastback) {
            hs.getRequest($scope.domain + "/translations").then(function (lexicon) {
                $scope.lexicon = lexicon;
                hs.getRequest($scope.domain + "/one/" + $stateParams.id).then(function (item) {
                    bus.emitGetRequest($scope, $scope.domain + ':one', item, item.Success);
                    lastback();
                });
            });
        });
    }

    function prepare() {
        setTimeout(function () {
            $.fn.editable.defaults.inputclass = 'form-control';
            $.fn.editable.defaults.mode = 'inline';
            $('#edit-from .formdata').changed(function (e) {
                var fieldName = $(e).attr("data-field");
                $scope.model[fieldName] = $(e).text();
            });
            $('#edit-from .formdata').each(function (i, el) {
                var defaultValue = $(el).attr("data-default-value");
                if (defaultValue) {
                    $(el).attr('disabled', true).click(function (e) { e.preventDefault(); });
                } else {
                    var type = $(el).attr("data-type");
                    var edobj = {
                        validate: function (value) {
                            var fieldName = $(this).attr("data-field");
                            var field = $scope.fieldmodel.filter(function (f) {
                                return f.name === fieldName;
                            })[0];
                            if (field.isRequired && $.trim(value) === '') {
                                return 'Bu alan lüzumludur';
                            }
                        },
                        autotext: 'Düzenleyin',
                        disabled: $scope.command === 'display-item'
                    };
                    switch (type) {
                        case 'text':
                            edobj = $.extend({
                                closeOnEnter: true
                            }, edobj);
                            break;
                        case 'select2':
                            var link = $(el).attr('data-link');
                            var selectables = $(el).attr('data-source');
                            
                            if (link) {
                                edobj = $.extend({
                                    select2: {
                                        width: $('#edit-container').width() * 0.35,
                                        allowClear: true,
                                        id: function (item) {
                                            return item.value;
                                        },
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        },
                                        ajax: {
                                            dataType: 'json',
                                            url: link,
                                            processResults: function (data, page) {
                                                return {
                                                    results: data
                                                };
                                            }
                                        }
                                    },
                                    tpl: '<select style="width:' + ($('#edit-container').width() * 0.35) + 'px;">',
                                    type: 'select2',
                                    success: function (response, newValue) {
                                        $(el).attr('data-pk', newValue);
                                    },
                                    display: function (value, sourceData) {
                                        console.log('display');
                                        console.log(value, sourceData);
                                        //set new text for xeditable field from php response 
                                        $(el).text(sourceData);
                                    }
                                }, edobj);
                            } else if (selectables) {
                                edobj = $.extend({
                                    select2: {
                                        width: $('#edit-container').width() * 0.35,
                                        allowClear: true,
                                        id: function (item) {
                                            return item.value;
                                        },
                                        formatResult: function (item) {
                                            return item.text;
                                        },
                                        formatSelection: function (item) {
                                            return item.text;
                                        },
                                        data: selectables
                                    },
                                    tpl: '<select style="width:' + ($('#edit-container').width() * 0.35) + 'px;">',
                                    type: 'select2',
                                    success: function (response, newValue) {
                                        $(el).attr('data-pk', newValue);
                                    },
                                    display: function (value, sourceData) {
                                        console.log('display');
                                        console.log(value, sourceData);
                                        //set new text for xeditable field from php response 
                                        $(el).text(sourceData);
                                    }
                                }, edobj);
                            }
                            break;
                        case 'combodate':
                            edobj = $.extend({
                                closeOnEnter: true
                            }, edobj)
                            break;
                        default:
                    }
                    $(el).editable(edobj);
                }
            });
            $('#edit-from').screen($('#edit-container'), $scope.command === 'display-item');
        }, 100);
    }

    $scope.submitEdit = function () {
        hs.postRequest($scope.domain + "/edit", $scope.model).then(function (data) {
            bus.emitPutRequest($scope, $scope.domain + ':put', data, data.ContainsItem);
        });
    };

    init();
}]);
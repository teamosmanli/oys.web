﻿App.directive("inject", ['FetchFactory', function (ff) {
    return {
        restrict: 'A',
        link: function ($scope, element, attributes) {
            $scope.injected = ff[attributes.ngGenerator]();
        }
    };
}]);
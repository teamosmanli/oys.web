﻿function SideMenuController($scope, $element, $attrs, MenuItemsFactory) {
    var ctrl = this;
    ctrl.menuitems = MenuItemsFactory();
}

App.component('sideMenu', {
    templateUrl: 'Assets/templates/subviews/sidemenu.html',
    controller: SideMenuController
});
﻿App.service('HttpService', function ($http, $log, $rootScope) {
    var service = this;
    service.getRequest = function (route, version) {
        version = version || "v1";
        return {
            then: function (callback) {
                $http.get('/Angular/Config/ApiConfig.json').success(function (cfg) {
                    var host = cfg.ProductionEnvironment ? cfg.Hosts.CurrentProductionHost : cfg.Hosts.CurrentDebugHost;
                    service.tokenize(host, function (session) {
                        $http.get(host + version + '/' + route, { headers: { "X-HDS-Tahdit": session.Token.AccessToken } })
                            .success(function (data) {
                                callback(data);
                            })
                            .error(function (data, code) {
                                callback(data);
                                $log.error(data, code);
                            });
                    }, service.session, version);
                });
            }
        };
    };
    service.postRequest = function (route, model, version) {
        version = version || "v1";
        return {
            then: function (callback) {
                $http.get('/Angular/Config/ApiConfig.json').success(function (cfg) {
                    var host = cfg.ProductionEnvironment ? cfg.Hosts.CurrentProductionHost : cfg.Hosts.CurrentDebugHost;
                    service.tokenize(host, function (session) {
                        $http.post(host + version + '/' + route, model, { headers: { "X-HDS-Tahdit": session.Token.AccessToken } })
                            .success(function (data) {
                                callback(data);
                            })
                            .error(function (data, code) {
                                callback(data);
                                $log.error(data, code);
                            });
                    }, service.session, version);
                });
            }
        };
    };
    service.deleteRequest = function (route, version) {
        version = version || "v1";
        return {
            then: function (callback) {
                $http.get('/Angular/Config/ApiConfig.json').success(function (cfg) {
                    var host = cfg.ProductionEnvironment ? cfg.Hosts.CurrentProductionHost : cfg.Hosts.CurrentDebugHost;
                    service.tokenize(host, function (session) {
                        $http.delete(host + version + '/' + route, { headers: { "X-HDS-Tahdit": session.Token.AccessToken } })
                            .success(function (data) {
                                callback(data);
                            })
                            .error(function (data, code) {
                                callback(data);
                                $log.error(data, code);
                            });
                    }, service.session, version);
                });
            }
        };
    };
    service.tokenize = function (host, callback, session, version) {
        if (service.session && service.session.Token.Success) {
            callback(service.session);
        } else {
            var url = host + version + '/aza/jeton';
            $http.post(url, session || { UserName: "25871077074", Password: "3132" })
                .success(function (data) {
                    service.session = data;
                    callback(service.session);
                })
                .error(function (data, code) {
                    service.session = data;
                    callback(service.session);
                    $log.error(msg, code);
                });
        }
    };
});
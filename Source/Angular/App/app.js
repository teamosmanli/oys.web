var App = angular.module('HDS', ['ngRoute', 'ui.bootstrap', 'ui.router', 'oc.lazyLoad', ]);
App.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/index");

    // Now set up the states
    $stateProvider
        .state('index', {
            url: "/index",
            templateUrl: 'Assets/templates/states/main.html',
            controller: 'MainController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                                'Assets/vendors/jquery-tablesorter/jquery.tablesorter.min.js',
                                'Assets/vendors/calendar/zabuto_calendar.min.js',
                                'Assets/vendors/flot-chart/jquery.flot.categories.js',
                                'Assets/vendors/flot-chart/jquery.flot.pie.js',
                                'Assets/vendors/flot-chart/jquery.flot.tooltip.js',
                                'Assets/vendors/flot-chart/jquery.flot.resize.js',
                                'Assets/vendors/flot-chart/jquery.flot.fillbetween.js',
                                'Assets/vendors/flot-chart/jquery.flot.stack.js',
                                'Assets/vendors/flot-chart/jquery.flot.spline.js']
                    });
                }]
            }
        })
        .state('layout-left-sidebar', {
            url: "/layout-left-sidebar",
            templateUrl: 'Assets/templates/states/layout-left-sidebar.html'
        })
        .state('layout-left-sidebar-collapsed', {
            url: "/layout-left-sidebar-collapsed",
            templateUrl: 'Assets/templates/states/layout-left-sidebar-collapsed.html'
        })
        .state('layout-right-sidebar', {
            url: "/layout-right-sidebar",
            templateUrl: 'Assets/templates/states/layout-right-sidebar.html'
        })
        .state('layout-right-sidebar-collapsed', {
            url: "/layout-right-sidebar-collapsed",
            templateUrl: 'Assets/templates/states/layout-right-sidebar-collapsed.html'
        })
        .state('layout-horizontal-menu', {
            url: "/layout-horizontal-menu",
            templateUrl: 'Assets/templates/states/layout-horizontal-menu.html'
        })
        .state('layout-horizontal-menu-sidebar', {
            url: "/layout-horizontal-menu-sidebar",
            templateUrl: 'Assets/templates/states/layout-horizontal-menu-sidebar.html'
        })
        .state('layout-fixed-topbar', {
            url: "/layout-fixed-topbar",
            templateUrl: 'Assets/templates/states/layout-fixed-topbar.html'
        })
        .state('layout-boxed', {
            url: "/layout-boxed",
            templateUrl: 'Assets/templates/states/layout-boxed.html'
        })
        .state('layout-hidden-footer', {
            url: "/layout-hidden-footer",
            templateUrl: 'Assets/templates/states/layout-hidden-footer.html'
        })
        .state('layout-header-topbar', {
            url: "/layout-header-topbar",
            templateUrl: 'Assets/templates/states/layout-header-topbar.html',
            controller: 'LayoutHeaderTopbarController'
        })
        .state('layout-title-breadcrumb', {
            url: "/layout-title-breadcrumb",
            templateUrl: 'Assets/templates/states/layout-title-breadcrumb.html',
            controller: 'LayoutTitleBreadcrumbController'
        })
        // start ui-element
        .state('ui-generals', {
            url: "/ui-generals",
            templateUrl: 'Assets/templates/states/ui-generals.html',
            controller: 'UiGeneralsController'
        })
        .state('ui-buttons', {
            url: "/ui-buttons",
            templateUrl: 'Assets/templates/states/ui-buttons.html'
        })
        .state('ui-tabs', {
            url: "/ui-tabs",
            templateUrl: 'Assets/templates/states/ui-tabs.html',
            controller: 'UiTabsController'
        })
        .state('ui-panels', {
            url: "/ui-panels",
            templateUrl: 'Assets/templates/states/ui-panels.html'
        })
        .state('ui-progressbars', {
            url: "/ui-progressbars",
            templateUrl: 'Assets/templates/states/ui-progressbars.html',
            controller: 'UiProgressbarsController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.1.1.min.css',
                                'Assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js']
                    });
                }]
            }
        })
        .state('ui-editors', {
            url: "/ui-editors",
            templateUrl: 'Assets/templates/states/ui-editors.html',
            controller: 'UiEditorsController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css',
                                'Assets/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
                                'Assets/vendors/summernote/summernote.css',
                                'Assets/vendors/bootstrap-markdown/js/bootstrap-markdown.js',
                                'Assets/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
                                'Assets/vendors/ckeditor/ckeditor.js',
                                'Assets/vendors/summernote/summernote.js', ]
                    });
                }]
            }
        })
        .state('ui-typography', {
            url: "/ui-typography",
            templateUrl: 'Assets/templates/states/ui-typography.html'
        })
        .state('ui-modals', {
            url: "/ui-modals",
            templateUrl: 'Assets/templates/states/ui-modals.html',
            controller: 'UIModalsController',
        })
        .state('ui-sliders', {
            url: "/ui-sliders",
            templateUrl: 'Assets/templates/states/ui-sliders.html',
            controller: 'UiSlidersController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css',
                                'Assets/vendors/nouislider/jquery.nouislider.css',
                                'Assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js',
                                'Assets/vendors/nouislider/jquery.nouislider.min.js']
                    });
                }]
            }
        })
        .state('ui-nestable-list', {
            url: "/ui-nestable-list",
            templateUrl: 'Assets/templates/states/ui-nestable-list.html',
            controller: 'UiNestableListController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-nestable/nestable.css',
                                'Assets/vendors/jquery-nestable/jquery.nestable.js']
                    });
                }]
            }
        })
        .state('ui-dropdown-select', {
            url: "/ui-dropdown-select",
            templateUrl: 'Assets/templates/states/ui-dropdown-select.html',
            controller: 'UiDropdownSelectController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/select2/css/select2.css',
                                'Assets/vendors/bootstrap-select/bootstrap-select.min.css',
                                'Assets/vendors/multi-select/css/multi-select-madmin.css',
                                'Assets/vendors/select2/js/select2.min.js',
                                'Assets/vendors/bootstrap-select/bootstrap-select.min.js',
                                'Assets/vendors/multi-select/js/jquery.multi-select.js']
                    });
                }]
            }
        })
        .state('ui-icons', {
            url: "/ui-icons",
            templateUrl: 'Assets/templates/states/ui-icons.html'
        })
        .state('ui-notific8-notifications', {
            url: "/ui-notific8-notifications",
            templateUrl: 'Assets/templates/states/ui-notific8-notifications.html',
            controller: 'UINotific8NotificationsController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-notific8/jquery.notific8.min.css',
                                'Assets/vendors/sco.message/sco.message.css',
                                'Assets/vendors/jquery-notific8/jquery.notific8.min.js',
                                'Assets/vendors/sco.message/sco.message.js',
                                'Assets/vendors/jquery-notific8/notific8.js']
                    });
                }]
            }
        })
        .state('ui-toastr-notifications', {
            url: "/ui-toastr-notifications",
            templateUrl: 'Assets/templates/states/ui-toastr-notifications.html',
            controller: 'UiToastrNotificationsController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-toastr/toastr.min.css',
                                'Assets/vendors/jquery-toastr/toastr.min.js']
                    });
                }]
            }
        })
        .state('ui-checkbox-radio', {
            url: "/ui-checkbox-radio",
            templateUrl: 'Assets/templates/states/ui-checkbox-radio.html',
            controller: 'CheckboxRadioController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/iCheck/skins/all.css',
                                'Assets/vendors/iCheck/icheck.min.js',
                                'Assets/vendors/iCheck/custom.min.js']
                    });
                }]
            }
        })
        .state('ui-treeview', {
            url: "/ui-treeview",
            templateUrl: 'Assets/templates/states/ui-treeview.html',
            controller: 'UiTreeviewController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jstree/dist/themes/default/style.min.css',
                                'Assets/vendors/jquery-treetable/stylesheets/jquery.treetable.css',
                                'Assets/vendors/jquery-treetable/stylesheets/jquery.treetable.theme.custom.css',
                                'Assets/vendors/jstree/dist/jstree.min.js',
                                'Assets/vendors/jquery-treetable/javascripts/src/jquery.treetable.js']
                    });
                }]
            }
        })
        .state('ui-portlets', {
            url: "/ui-portlets",
            templateUrl: 'Assets/templates/states/ui-portlets.html',
            controller: 'UiPortletsController'
        })

        // start form
        .state('form-layouts', {
            url: "/form-layouts",
            templateUrl: 'Assets/templates/states/form-layouts.html',
            controller: 'FormLayoutsController'
        })
        .state('form-basic', {
            url: "/form-basic",
            templateUrl: 'Assets/templates/states/form-basic.html',
            controller: 'FormBasicController'
        })
        .state('form-components', {
            url: "/form-components",
            templateUrl: 'Assets/templates/states/form-components.html',
            controller: 'FormComponentsController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-colorpicker/css/colorpicker.css',
                                'Assets/vendors/bootstrap-datepicker/css/datepicker.css',
                                'Assets/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                'Assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                'Assets/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                                'Assets/vendors/bootstrap-clockface/css/clockface.css',
                                'Assets/vendors/bootstrap-switch/css/bootstrap-switch.css',
                                'Assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                                'Assets/vendors/bootstrap-daterangepicker/daterangepicker.js',
                                'Assets/vendors/moment/moment.js',
                                'Assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                'Assets/vendors/bootstrap-timepicker/js/bootstrap-timepicker.js',
                                'Assets/vendors/bootstrap-clockface/js/clockface.js',
                                'Assets/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                                'Assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js',
                                'Assets/vendors/jquery-maskedinput/jquery-maskedinput.js',
                                'Assets/vendors/charCount.js']
                    });
                }]
            }
        })
        .state('form-wizard', {
            url: "/form-wizard",
            templateUrl: 'Assets/templates/states/form-wizard.html',
            controller: 'FormWizardController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-bootstrap-wizard/custom.css',
                                'Assets/vendors/jquery-steps/css/jquery.steps.css',
                                'Assets/vendors/jquery-validate/jquery.validate.min.js',
                                'Assets/vendors/jquery-steps/js/jquery.steps.min.js',
                                'Assets/vendors/jquery-bootstrap-wizard/jquery.bootstrap.wizard.min.js']
                    });
                }]
            }
        })
        .state('form-xeditable', {
            url: "/form-xeditable",
            templateUrl: 'Assets/templates/states/form-xeditable.html',
            controller: 'FormXeditableController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                'Assets/vendors/x-editable/bootstrap3-editable/css/bootstrap-editable.css',
                                'Assets/vendors/x-editable/inputs-ext/address/address.css',
                                'Assets/vendors/bootstrap-switch/css/bootstrap-switch.css',
                                'Assets/vendors/x-editable/jquery.mockjax.js',
                                'Assets/vendors/moment/moment.js',
                                'Assets/vendors/select2/js/select2.js',
                                'Assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                'Assets/vendors/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js',
                                'Assets/vendors/x-editable/inputs-ext/typeaheadjs/lib/typeahead.js',
                                'Assets/vendors/x-editable/inputs-ext/typeaheadjs/typeaheadjs.js',
                                'Assets/vendors/x-editable/inputs-ext/wysihtml5/wysihtml5.js',
                                'Assets/vendors/x-editable/inputs-ext/address/address.js',
                                'Assets/vendors/bootstrap-switch/js/bootstrap-switch.min.js',
                                'Assets/vendors/x-editable/demo-mock.js']
                    });
                }]
            }
        })
        .state('form-validation', {
            url: "/form-validation",
            templateUrl: 'Assets/templates/states/form-validation.html',
            controller: 'FormValidationController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-validate/jquery.validate.min.js']
                    });
                }]
            }
        })
        .state('form-multiple-file-upload', {
            url: "/form-multiple-file-upload",
            templateUrl: 'Assets/templates/states/form-multiple-file-upload.html',
            controller: 'FormMultipleFileUploadController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-file-upload/css/jquery.fileupload.css',
                                'Assets/vendors/jquery-file-upload/css/jquery.fileupload-ui.css',
                                'Assets/vendors/jquery-file-upload/css/blueimp-gallery.min.css',
                                'Assets/vendors/jquery-file-upload/js/vendor/jquery.ui.widget.js',
                                'Assets/vendors/jquery-file-upload/js/vendor/tmpl.min.js',
                                'Assets/vendors/jquery-file-upload/js/vendor/load-image.min.js',
                                'Assets/vendors/jquery-file-upload/js/vendor/canvas-to-blob.min.js',
                                'Assets/vendors/jquery-file-upload/js/vendor/jquery.blueimp-gallery.min.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.iframe-transport.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload-process.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload-image.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload-audio.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload-video.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload-validate.js',
                                'Assets/vendors/jquery-file-upload/js/jquery.fileupload-ui.js',
                                'Assets/vendors/jquery-file-upload/js/cors/jquery.xdr-transport.js']
                    });
                }]
            }
        })
        .state('form-dropzone-file-upload', {
            url: "/form-dropzone-file-upload",
            templateUrl: 'Assets/templates/states/form-dropzone-file-upload.html',
            controller: 'FormDropzoneFileUploadController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/dropzone/css/dropzone.css',
                                'Assets/vendors/dropzone/js/dropzone.js']
                    });
                }]
            }
        })

        // start front end
        .state('frontend-one-page', {
            url: "/frontend-one-page",
            templateUrl: 'Assets/templates/states/frontend-one-page.html'
        })

        // start table
        .state('table-basic', {
            url: "/table-basic",
            templateUrl: 'Assets/templates/states/table-basic.html'
        })
        .state('table-responsive', {
            url: "/table-responsive",
            templateUrl: 'Assets/templates/states/table-responsive.html'
        })
        .state('table-action', {
            url: "/table-action",
            templateUrl: 'Assets/templates/states/table-action.html',
            controller: 'TableActionController'
        })
        .state('table-filter', {
            url: "/table-filter",
            templateUrl: 'Assets/templates/states/table-filter.html',
            controller: 'TableFilterController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-datepicker/css/datepicker3.css',
                                'Assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js']
                    });
                }]
            }
        })
        .state('table-advanced', {
            url: "/table-advanced",
            templateUrl: 'Assets/templates/states/table-advanced.html',
            controller: 'TableAdvancedController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-tablesorter/themes/blue/style-custom.css',
                                'Assets/vendors/jquery-tablesorter/jquery.tablesorter.js']
                    });
                }]
            }
        })
        .state('table-editable', {
            url: "/table-editable",
            templateUrl: 'Assets/templates/states/table-editable.html',
            controller: 'TableEditableController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                           'Assets/vendors/DataTables/media/css/jquery.dataTables.css',
                           'Assets/vendors/DataTables/media/css/dataTables.bootstrap.css',
                           'Assets/vendors/DataTables/media/js/jquery.dataTables.js',
                           'Assets/vendors/DataTables/media/js/dataTables.bootstrap.js',
                           'Assets/vendors/DataTables/jquery.jeditable.js'
                        ]
                    });
                }]
            }
        })
        .state('table-datatables', {
            url: "/table-datatables",
            templateUrl: 'Assets/templates/states/table-datatables.html',
            controller: 'TableDatatablesController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                               'Assets/vendors/DataTables/media/css/jquery.dataTables.css',
                               'Assets/vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css',
                               'Assets/vendors/DataTables/media/css/dataTables.bootstrap.css',
                               'Assets/vendors/DataTables/media/js/jquery.dataTables.js',
                               'Assets/vendors/DataTables/media/js/dataTables.bootstrap.js',
                               'Assets/vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js'
                        ]
                    });
                }]
            }
        })
        .state('table-sample', {
            url: "/table-sample",
            templateUrl: 'Assets/templates/states/table-sample.html',
            controller: 'TableSampleController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-tablesorter/themes/blue/style-custom.css',
                                'Assets/vendors/bootstrap-datepicker/css/datepicker.css',
                                'Assets/vendors/jquery-tablesorter/jquery.tablesorter.js',
                                'Assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js']
                    });
                }]
            }
        })
        .state('table-export', {
            url: "/table-export",
            templateUrl: 'Assets/templates/states/table-export.html',
            controller: 'TableExportController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/tableExport/js/tableexport.js',
                                'Assets/vendors/tableExport/jquery.base64.js',
                                'Assets/vendors/tableExport/html2canvas.js',
                                'Assets/vendors/tableExport/jspdf/libs/sprintf.js',
                                'Assets/vendors/tableExport/jspdf/jspdf.js',
                                'Assets/vendors/tableExport/jspdf/libs/base64.js']
                    });
                }]
            }
        })

        // start data grid
        .state('grid-layout-div', {
            url: "/grid-layout-div",
            templateUrl: 'Assets/templates/states/grid-layout-div.html',
            controller: 'GridLayoutDivController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-layout-table-1', {
            url: "/grid-layout-table-1",
            templateUrl: 'Assets/templates/states/grid-layout-table-1.html',
            controller: 'GridLayoutTable1Controller',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-layout-table-2', {
            url: "/grid-layout-table-2",
            templateUrl: 'Assets/templates/states/grid-layout-table-2.html',
            controller: 'GridLayoutTable2Controller',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-layout-2-table', {
            url: "/grid-layout-2-table",
            templateUrl: 'Assets/templates/states/grid-layout-2-table.html',
            controller: 'GridLayout2TableController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-layout-ul-li', {
            url: "/grid-layout-ul-li",
            templateUrl: 'Assets/templates/states/grid-layout-ul-li.html',
            controller: 'GridLayoutUlLiController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-filter-with-ul-li', {
            url: "/grid-filter-with-ul-li",
            templateUrl: 'Assets/templates/states/grid-filter-with-ul-li.html',
            controller: 'GridFilterWithUiLiController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-filter-with-select', {
            url: "/grid-filter-with-select",
            templateUrl: 'Assets/templates/states/grid-filter-with-select.html',
            controller: 'GridFilterWithSelectController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-double-sort', {
            url: "/grid-double-sort",
            templateUrl: 'Assets/templates/states/grid-double-sort.html',
            controller: 'GridDoubleSortController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-deep-linking', {
            url: "/grid-deep-linking",
            templateUrl: 'Assets/templates/states/grid-deep-linking.html',
            controller: 'GridDeepLinkingController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })

        .state('grid-pagination-only', {
            url: "/grid-pagination-only",
            templateUrl: 'Assets/templates/states/grid-pagination-only.html',
            controller: 'GridPaginationOnlyController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-without-item-per-page', {
            url: "/grid-without-item-per-page",
            templateUrl: 'Assets/templates/states/grid-without-item-per-page.html',
            controller: 'GridWithoutItemPerPageController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-hidden-sort', {
            url: "/grid-hidden-sort",
            templateUrl: 'Assets/templates/states/grid-hidden-sort.html',
            controller: 'GridHiddenSortController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-range-slider', {
            url: "/grid-range-slider",
            templateUrl: 'Assets/templates/states/grid-range-slider.html',
            controller: 'GridRangeSliderController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })
        .state('grid-datepicker', {
            url: "/grid-datepicker",
            templateUrl: 'Assets/templates/states/grid-datepicker.html',
            controller: 'GridDatepickerController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jplist/html/css/jplist-custom.css',
                                'Assets/vendors/jplist/html/js/vendor/modernizr.min.js',
                                'Assets/vendors/jplist/html/js/jplist.min.js']
                    });
                }]
            }
        })

        // start page
        .state('page-gallery', {
            url: "/page-gallery",
            templateUrl: 'Assets/templates/states/page-gallery.html',
            controller: 'PageGalleryController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/lightbox/css/lightbox.css',
                                'Assets/vendors/mixitup/src/jquery.mixitup.js',
                                'Assets/vendors/lightbox/js/lightbox.min.js']
                    });
                }]
            }
        })
        .state('page-timeline', {
            url: "/page-timeline",
            templateUrl: 'Assets/templates/states/page-timeline.html'
        })
        .state('page-blog', {
            url: "/page-blog",
            templateUrl: 'Assets/templates/states/page-blog.html'
        })
        .state('page-blog-item', {
            url: "/page-blog-item",
            templateUrl: 'Assets/templates/states/page-blog-item.html'
        })
        .state('page-about', {
            url: "/page-about",
            templateUrl: 'Assets/templates/states/page-about.html'
        })
        .state('page-contact', {
            url: "/page-contact",
            templateUrl: 'Assets/templates/states/page-contact.html',
            controller: 'PageContactController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/gmaps/gmaps.js']
                    });
                }]
            }
        })
        .state('page-calendar', {
            url: "/page-calendar",
            templateUrl: 'Assets/templates/states/page-calendar.html',
            controller: 'PageCalendarController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/fullcalendar/fullcalendar.css',
                                'Assets/vendors/fullcalendar/fullcalendar.print.css',
                                'Assets/vendors/fullcalendar/fullcalendar.min.js']
                    });
                }]
            }
        })

        // start extra
        .state('extra-profile', {
            url: "/extra-profile",
            templateUrl: 'Assets/templates/states/extra-profile.html',
            controller: 'ExtraProfileController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-datepicker/css/datepicker.css',
                                'Assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js']
                    });
                }]
            }
        })
        .state('extra-signin', {
            url: "/extra-signin",
            templateUrl: 'Assets/templates/states/extra-signin.html',
            controller: 'ExtraSigninController'
        })
        .state('extra-signup', {
            url: "/extra-signup",
            templateUrl: 'Assets/templates/states/extra-signup.html',
            controller: 'ExtraSignupController'
        })
        .state('extra-lock-screen', {
            url: "/extra-lock-screen",
            templateUrl: 'Assets/templates/states/extra-lock-screen.html',
            controller: 'ExtraLockScreenController'
        })
        .state('extra-user-list', {
            url: "/extra-user-list",
            templateUrl: 'Assets/templates/states/extra-user-list.html',
            controller: 'ExtraUserListController'
        })
        .state('extra-invoice', {
            url: "/extra-invoice",
            templateUrl: 'Assets/templates/states/extra-invoice.html'
        })
        .state('extra-faq', {
            url: "/extra-faq",
            templateUrl: 'Assets/templates/states/extra-faq.html'
        })
        .state('extra-pricing-table', {
            url: "/extra-pricing-table",
            templateUrl: 'Assets/templates/states/extra-pricing-table.html'
        })
        .state('extra-blank', {
            url: "/extra-blank",
            templateUrl: 'Assets/templates/states/extra-blank.html'
        })
        .state('extra-404', {
            url: "/extra-404",
            templateUrl: 'Assets/templates/states/extra-404.html',
            controller: 'Extra404Controller'
        })
        .state('extra-500', {
            url: "/extra-500",
            templateUrl: 'Assets/templates/states/extra-500.html',
            controller: 'Extra500Controller'
        })

        // start email
        .state('email-inbox', {
            url: "/email-inbox",
            templateUrl: 'Assets/templates/states/email-inbox.html',
            controller: 'EmailInboxController'
        })
        .state('email-compose-mail', {
            url: "/email-compose-mail",
            templateUrl: 'Assets/templates/states/email-compose-mail.html',
            controller: 'EmailComposeMailController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
                                'Assets/vendors/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js']
                    });
                }]
            }
        })
        .state('email-view-mail', {
            url: "/email-view-mail",
            templateUrl: 'Assets/templates/states/email-view-mail.html'
        })

        // start charts
        .state('charts-flotchart', {
            url: "/charts-flotchart",
            templateUrl: 'Assets/templates/states/charts-flotchart.html',
            controller: 'ChartsFlotChartController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/flot-chart/jquery.flot.js',
                                'Assets/vendors/flot-chart/jquery.flot.categories.js',
                                'Assets/vendors/flot-chart/jquery.flot.pie.js',
                                'Assets/vendors/flot-chart/jquery.flot.tooltip.js',
                                'Assets/vendors/flot-chart/jquery.flot.resize.js',
                                'Assets/vendors/flot-chart/jquery.flot.fillbetween.js',
                                'Assets/vendors/flot-chart/jquery.flot.stack.js',
                                'Assets/vendors/flot-chart/jquery.flot.spline.js']
                    });
                }]
            }
        })
        .state('charts-chartjs', {
            url: "/charts-chartjs",
            templateUrl: 'Assets/templates/states/charts-chartjs.html',
            controller: 'ChartsChartJsController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/chart.js/Chart.min.js']
                    });
                }]
            }
        })
        .state('charts-highchart-line', {
            url: "/charts-highchart-line",
            templateUrl: 'Assets/templates/states/charts-highchart-line.html',
            controller: 'ChartsHighchartLineController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-area', {
            url: "/charts-highchart-area",
            templateUrl: 'Assets/templates/states/charts-highchart-area.html',
            controller: 'ChartsHighchartAreaController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/highcharts-more.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-column-bar', {
            url: "/charts-highchart-column-bar",
            templateUrl: 'Assets/templates/states/charts-highchart-column-bar.html',
            controller: 'ChartsHighchartColumnBarController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/highcharts-more.js',
                                'Assets/vendors/jquery-highcharts/data.js',
                                'Assets/vendors/jquery-highcharts/drilldown.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-pie', {
            url: "/charts-highchart-pie",
            templateUrl: 'Assets/templates/states/charts-highchart-pie.html',
            controller: 'ChartsHighchartPieController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/data.js',
                                'Assets/vendors/jquery-highcharts/drilldown.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-scatter-bubble', {
            url: "/charts-highchart-scatter-bubble",
            templateUrl: 'Assets/templates/states/charts-highchart-scatter-bubble.html',
            controller: 'ChartsHighchartScatterBubbleController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/highcharts-more.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-dynamic', {
            url: "/charts-highchart-dynamic",
            templateUrl: 'Assets/templates/states/charts-highchart-dynamic.html',
            controller: 'ChartsHighchartDynamicController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-combinations', {
            url: "/charts-highchart-combinations",
            templateUrl: 'Assets/templates/states/charts-highchart-combinations.html',
            controller: 'ChartsHighchartCombinationController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highcharts.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })
        .state('charts-highchart-more', {
            url: "/charts-highchart-more",
            templateUrl: 'Assets/templates/states/charts-highchart-more.html',
            controller: 'ChartsHighchartMoreController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: ['Assets/vendors/jquery-highcharts/highchart.js',
                                'Assets/vendors/jquery-highcharts/funnel.js',
                                'Assets/vendors/jquery-highcharts/highcharts-more.js',
                                'Assets/vendors/jquery-highcharts/data.js',
                                'Assets/vendors/jquery-highcharts/exporting.js']
                    });
                }]
            }
        })

        // start animation
        .state('animations', {
            url: "/animations",
            templateUrl: 'Assets/templates/states/animations.html'
        })

        //start custom states
        .state('items-table', {
            url: "/items-table/:domain/:command",
            params: {
                domain: "serviceorders",
                command: "list-items"
            },
            templateUrl: 'Angular/Templates/ItemsTable.html',
            controller: 'ItemsTableController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                                'Assets/vendors/DataTables/media/css/jquery.dataTables.css',
                                'Assets/vendors/DataTables/extensions/TableTools/css/dataTables.tableTools.min.css',
                                'Assets/vendors/DataTables/media/css/dataTables.bootstrap.css',
                                'Assets/vendors/DataTables/media/js/jquery.dataTables.js',
                                'Assets/vendors/DataTables/media/js/dataTables.bootstrap.js',
                                'Assets/vendors/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js',
                                'Assets/vendors/tableExport/js/tableexport.js',
                                'Assets/vendors/tableExport/jquery.base64.js',
                                'Assets/vendors/tableExport/jspdf/libs/sprintf.js',
                                'Assets/vendors/tableExport/jspdf/jspdf.js',
                                'Assets/vendors/tableExport/jspdf/libs/base64.js'
                        ]
                    });
                }]
            }
        })
        .state('edit-form', {
            url: "/edit-form/:domain/:id/:command",
            params: {
                domain: "serviceorders",
                id: 0,
                command: "display-item"
            },
            templateUrl: 'Angular/Templates/EditItemFrame.html',
            controller: 'EditItemController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            //'Assets/vendors/x-editable/select2/lib/select2-madmin.css',
                            'Assets/vendors/select2/css/select2.min.css',
                            'Assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                            'Assets/vendors/x-editable/bootstrap3-editable/css/bootstrap-editable.css',
                            'Assets/vendors/x-editable/inputs-ext/address/address.css',
                            'Assets/vendors/bootstrap-switch-3.0/css/bootstrap3/bootstrap-switch.css',
                            'Assets/vendors/x-editable/jquery.mockjax.js',
                            //'Assets/vendors/moment/moment.js',
                            'Assets/vendors/moment/moment-with-locales.js',
                            //'Assets/vendors/x-editable/select2/lib/select2.js',
                            'Assets/vendors/select2/js/select2.full.min.js',
                            'Assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                            'Assets/vendors/x-editable/bootstrap3-editable/js/bootstrap-editable.min.js',
                            'Assets/vendors/bootstrap-switch-3.0/js/bootstrap-switch.min.js'
                        ]
                    });
                }]
            }
        })
        // start form
        .state('insert-form', {
            url: "/insert-form/:domain/:command",
            params: {
                domain: "azalar",
                command: "display-item"
            },
            templateUrl: 'Angular/Templates/InsertItemFrame.html',
            controller: 'InsertItemController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            'Assets/vendors/select2/css/select2.min.css',
                            'Assets/vendors/bootstrap-datetimepicker/bootstrap/bootstrap-datetimepicker.min.css',
                            'Assets/vendors/moment/moment-with-locales.js',
                            'Assets/vendors/select2/js/select2.full.min.js',
                            'Assets/vendors/bootstrap-datetimepicker/bootstrap/bootstrap-datetimepicker.min.js'
                        ]
                    });
                }]
            }
        })
    ;
});
